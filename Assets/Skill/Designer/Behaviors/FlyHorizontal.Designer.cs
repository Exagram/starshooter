using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Skill.Framework.AI;

public abstract class FlyHorizontal : Skill.Framework.AI.BehaviorTree
{

// Internal Enumurators
protected enum Actions
{
BasicMove = 0,
BasicShoot = 1,
}

// Internal class
public static class AccessKeys
{

// Variables

// Properties

// Methods
 static   AccessKeys()
{
}

}
public static class StateNames
{

// Variables
public static string _Root = "Root";

// Properties

// Methods

}

// Variables
private  Skill.Framework.AI.BehaviorTreeState _Root = null;
private  Skill.Framework.AI.ConcurrentSelector _NewConcurrentSelector2 = null;
private  Skill.Framework.AI.Action _BasicMove = null;
private  Skill.Framework.AI.Action _BasicShoot = null;

// Properties
public  override string DefaultState { get { return "Root"; } }

// Methods
private   Skill.Framework.AI.BehaviorResult BasicMove_Action(object sender, Skill.Framework.AI.BehaviorParameterCollection parameters)
{
return OnAction( Actions.BasicMove , sender ,  parameters);
}
private   Skill.Framework.AI.BehaviorResult BasicShoot_Action(object sender, Skill.Framework.AI.BehaviorParameterCollection parameters)
{
return OnAction( Actions.BasicShoot , sender ,  parameters);
}
protected  abstract Skill.Framework.AI.BehaviorResult OnAction(Actions action, object sender, Skill.Framework.AI.BehaviorParameterCollection parameters);
protected  abstract void OnActionReset(Actions action);
protected  override Skill.Framework.AI.BehaviorTreeState[] CreateTree()
{
this._Root = new Skill.Framework.AI.BehaviorTreeState("Root");
this._NewConcurrentSelector2 = new Skill.Framework.AI.ConcurrentSelector("NewConcurrentSelector2");
this._BasicMove = new Skill.Framework.AI.Action("BasicMove", BasicMove_Action, Skill.Framework.Posture.Unknown);
this._BasicShoot = new Skill.Framework.AI.Action("BasicShoot", BasicShoot_Action, Skill.Framework.Posture.Unknown);

this._Root.Add(_NewConcurrentSelector2,null);
this._NewConcurrentSelector2.Add(_BasicMove,null);
this._NewConcurrentSelector2.Add(_BasicShoot,null);
Skill.Framework.AI.BehaviorTreeState[] states = new Skill.Framework.AI.BehaviorTreeState[1];
states[0] = _Root;
return states;

}

}

