﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {
	
	GUIStyle txtStyle = new GUIStyle();
	int bx, by;
	Texture2D wpn1, wpn2, wpn3, arrow;
	GUIStyle hpBgStyle;
	GUIStyle hpFgStyle;
	string announce = "";
	float announceLife = 0;

	void Awake() {
		txtStyle.normal.textColor = Color.white;
		txtStyle.fontSize = 22;

		Texture2D hpBgTexture = new Texture2D(1, 1);
		hpBgTexture.SetPixel(0, 0, Color.black);
		hpBgTexture.wrapMode = TextureWrapMode.Repeat;
		hpBgTexture.Apply();

		hpBgStyle = new GUIStyle();
		hpBgStyle.normal.background = hpBgTexture;

		Texture2D hpFgTexture = new Texture2D(1, 1);
		hpFgTexture.SetPixel(0, 0, Color.green);
		hpFgTexture.wrapMode = TextureWrapMode.Repeat;
		hpFgTexture.Apply();

		hpFgStyle = new GUIStyle();
		hpFgStyle.normal.background = hpFgTexture;
	}

	void Start () {
		wpn1 = (Texture2D)Resources.Load("UI/hud-wpn-1");
		wpn2 = (Texture2D)Resources.Load("UI/hud-wpn-2");
		wpn3 = (Texture2D)Resources.Load("UI/hud-wpn-3");
		arrow = (Texture2D)Resources.Load("UI/hud-arrow");
	}
	
	void OnGUI() {
		bx = 10;
		by = 10;

		GUI.TextField(new Rect(200, 200, 400, 20), Input.GetAxis("P1.Horizontal") + ", " + Input.GetAxis("P1.Vertical"), txtStyle);

		foreach(var player in GameManager.players) {
			var behavior = player.GetComponent<Player>();

			var lifeRatio = (behavior.lifeComponent.hp / behavior.lifeComponent.maxHP);

			var hpFgTexture = hpFgStyle.normal.background;
			hpFgTexture.SetPixel(0, 0, new Color(1 - lifeRatio, lifeRatio, 0));
			hpFgTexture.wrapMode = TextureWrapMode.Repeat;
			hpFgTexture.Apply();

			GUI.Box(new Rect(bx, by, 255, 20), GUIContent.none, hpBgStyle);
			GUI.Box(new Rect(bx, by, lifeRatio * 255, 20), GUIContent.none, hpFgStyle);

			by += 30;
		}

		foreach(var player in GameManager.players) {
			var behavior = player.GetComponent<Player>();

			GUI.DrawTexture(new Rect(bx, by, wpn1.width / 2, wpn1.height / 2), wpn1);
			bx += 40;
			GUI.Label(new Rect(bx, by, 30, 20), "∞", txtStyle);
			bx += 40;

			GUI.DrawTexture(new Rect(bx, by, wpn2.width / 2, wpn2.height / 2), wpn2);
			bx += 40;
			GUI.Label(new Rect(bx, by, 30, 20), behavior.weaponManager.weaponList[1].ammo + "", txtStyle);
			bx += 40;
			
			GUI.DrawTexture(new Rect(bx, by, wpn3.width / 2, wpn3.height / 2), wpn3);
			bx += 40;
			GUI.Label(new Rect(bx, by, 30, 20), behavior.weaponManager.weaponList[2].ammo + "", txtStyle);

			bx = 10;
			by += 30;

			GUI.DrawTexture(new Rect(bx + behavior.weaponIndex * 80, by, arrow.width / 2, arrow.height / 2), arrow);

			by += 30;
		}

		if (announceLife > 0) {
			bx = Screen.width / 2;
			by = Screen.height / 2;
			GUI.Label(new Rect(bx - announce.Length * 2, by - 5, announce.Length * 5, 20), announce, txtStyle);
			announceLife -= Time.deltaTime;
		}
	}

	public void Announce(string a, float life) {
		announce = a;
		announceLife = life;
	}
}
