﻿using UnityEngine;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {

	public List<AudioClip> musicList = new List<AudioClip>();
	public List<AudioClip> sfxList = new List<AudioClip>();
	public int musicIndex = -1;
	public bool isPlaying = false;

	public void Awake() {

		gameObject.AddComponent<AudioSource>();
		audio.panLevel = 0;

		var musicListR = Resources.LoadAll("Music");
		foreach(AudioClip item in musicListR) {
			musicList.Add(item);
		}
		
		var sfxListR = Resources.LoadAll("SFX", typeof(AudioClip));
		foreach (AudioClip item in sfxListR) {
			sfxList.Add(item);
		}
	}

	volatile bool a = true;
	public void Update() {
		if(a)
			return;
		if (isPlaying) {
			if (!audio.isPlaying) {
				musicIndex = (musicIndex + 1) % musicList.Count;
				audio.clip = musicList[musicIndex];
				audio.Play();
			}
		}
	}
}
