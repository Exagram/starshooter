﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class LetterBox : MonoBehaviour {

	const float KeepRatio = 16.0f / 9.0f;

	// Use this for initialization
	void Start () {
		float aspectRatio = Screen.width / ((float)Screen.height);
		float percentage = 1 - (aspectRatio / KeepRatio);
		
		camera.rect = new Rect(0f, (percentage / 2), 1f, (1 - percentage));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
