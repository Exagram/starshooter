using UnityEngine;
using System.Collections;
using Skill.Framework.AI;

public class EnemyWaspHunter : MonoBehaviour {

	WeaponManager weaponManager;

	void Awake() {
		weaponManager = GetComponent(typeof(WeaponManager)) as WeaponManager;
		weaponManager.add(new WeaponManager.Weapon {
			damageAmount = 1f,
			rechargeAmountMax = 2f,
			bulletObject = GameManager.prefabBulletEnemy,
			shootEvent = (Object parent, Vector3 position, Quaternion rotation) => {
				var gameObject = Instantiate(GameManager.prefabBulletEnemy, position, rotation) as GameObject;
				var homing = gameObject.GetComponent<HomingEnemyBullet>();
				homing.homingSpeed = 0;
			}
		});
	}

	void onDeath() {
		audio.clip = SFXManager.find(SFXManager.ALL.ExplosionEnemy);
		audio.Play();
		DestroyObject(gameObject);
	}

	void OnDestroy() {
		GameManager.enemies.Remove(gameObject);
	}
}
