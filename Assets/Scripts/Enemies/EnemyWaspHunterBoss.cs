using UnityEngine;
using System.Collections;

public class EnemyWaspHunterBoss : MonoBehaviour {

	WeaponManager weaponManager;
	float superMode = 7f;

	void Awake() {
		var wpn1 = new WeaponManager.Weapon {
			ammo = WeaponManager.AMMO_INFINITE,
			damageAmount = 1f,
			rechargeAmountMax = 1f,
			bulletObject = GameManager.prefabBulletEnemy,
		};
		wpn1.shootEvent = (parent, position, rotation) => {
			//bullet.GetComponent<EnemyBullet>().homingSpeed = 8f;

			GameObject.Instantiate(wpn1.bulletObject, position + 0.5f * Vector3.up, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, 5)));
			GameObject.Instantiate(wpn1.bulletObject, position, rotation);
			GameObject.Instantiate(wpn1.bulletObject, position + 0.5f * Vector3.down, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, -5)));
		};

		weaponManager = GetComponent(typeof(WeaponManager)) as WeaponManager;
		weaponManager.add(wpn1);
	}

	void Update() {
		if (superMode > 0) {
			superMode -= Time.deltaTime;
			return;
		}

		weaponManager.weaponList.Clear();

		var wpn1 = new WeaponManager.Weapon {
			ammo = WeaponManager.AMMO_INFINITE,
			damageAmount = 1f,
			rechargeAmountMax = 1f,
			bulletObject = GameManager.prefabBulletEnemy,
		};
		wpn1.shootEvent = (parent, position, rotation) => {
			
			//var bullet = GameObject.Instantiate(wpn1.bulletObject, position + 0.5f * Vector3.up, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, 6))) as GameObject;
			//bullet.GetComponent<EnemyBullet>().speed = 8f;
			//bullet.GetComponent<EnemyBullet>().homingSpeed = 8f;

			var bullet = GameObject.Instantiate(wpn1.bulletObject, position + 0.5f * Vector3.up, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, 3))) as GameObject;
			bullet.GetComponent<HomingEnemyBullet>().speed = 10f;
			bullet.GetComponent<HomingEnemyBullet>().homingSpeed = 8f;

			bullet = GameObject.Instantiate(wpn1.bulletObject, position, rotation) as GameObject;
			bullet.GetComponent<HomingEnemyBullet>().speed = 8f;
			bullet.GetComponent<HomingEnemyBullet>().homingSpeed = 8f;

			bullet = GameObject.Instantiate(wpn1.bulletObject, position + 0.5f * Vector3.down, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, -3))) as GameObject;
			bullet.GetComponent<HomingEnemyBullet>().speed = 10f;
			bullet.GetComponent<HomingEnemyBullet>().homingSpeed = 8f;

			//bullet = GameObject.Instantiate(wpn1.bulletObject, position + 0.5f * Vector3.down, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, -6))) as GameObject;
			//bullet.GetComponent<EnemyBullet>().speed = 8f;
			//bullet.GetComponent<EnemyBullet>().homingSpeed = 8f;
		};
		weaponManager.add(wpn1);

		superMode = 10000000f;
	}

	void onDeath() {
		audio.clip = SFXManager.find(SFXManager.ALL.ExplosionEnemy);
		audio.Play();

		foreach(var player in GameManager.players) {
			player.transform.position = new Vector3(
				-UnityEngine.Mathf.Abs(player.transform.position.x), 0, 0
				);
		}
		
		GameManager.changeScenes("Scene2");
	}

	void OnDestroy() {
		GameManager.enemies.Remove(gameObject);
	}
}
