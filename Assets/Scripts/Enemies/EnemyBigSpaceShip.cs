using UnityEngine;
using System.Collections;

public class EnemyBigSpaceShip : MonoBehaviour {

	WeaponManager weaponManager;

	void Awake() {
		var wpn1 = new WeaponManager.Weapon{
			damageAmount = 1f,
			rechargeAmountMax = 1f,
			bulletObject = GameManager.prefabBulletEnemy,
		};
		wpn1.shootEvent = (parent, position, rotation) => {
			var bullet = GameObject.Instantiate(wpn1.bulletObject, position, rotation) as GameObject;
			
			if (bullet != null) {
				bullet.GetComponent<HomingEnemyBullet>().homingSpeed = 8f;
			}
		};

		weaponManager = GetComponent(typeof(WeaponManager)) as WeaponManager;
		weaponManager.add(wpn1);
	}

	void onDeath() {
		audio.clip = SFXManager.find(SFXManager.ALL.ExplosionEnemy);
		audio.Play();
		DestroyObject(gameObject);
	}

	void OnDestroy() {
		GameManager.enemies.Remove(gameObject);
	}
}
