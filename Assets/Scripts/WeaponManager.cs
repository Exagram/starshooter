using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour {
	
	public static int AMMO_INFINITE = -1;

	public delegate void ShootEvent(Object parent, Vector3 position, Quaternion rotation);

	public class Weapon {

		public GameObject bulletObject;

		public int ammo;
		public float damageAmount = 0f;
		public float rechargeAmount = 0f;
		public float rechargeAmountMax = 0f;
		public ShootEvent shootEvent;

		public Weapon() {
			ammo = AMMO_INFINITE;
			damageAmount = 1f;
			rechargeAmountMax = 0.7f;
			//this.bulletObject = bulletObject;

			shootEvent = (Object parent, Vector3 position, Quaternion rotation) => {
				Instantiate(bulletObject, position, rotation);
			};
		}

		public void update(float amount) {
			rechargeAmount = Mathf.Clamp(rechargeAmount - amount, 0, rechargeAmountMax);
		}
	}
	
	public List<Weapon> weaponList = new List<Weapon>();
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		foreach(var weapon in weaponList) {
			weapon.update(Time.deltaTime);
		}
	}
	
	public void add(Weapon weapon) {
		weaponList.Add(weapon);
	}
	
	public bool isReady(int index) {
		
		if(index >= weaponList.Count) {
			return false;
		}

		if (weaponList[index].ammo <= 0 && weaponList[index].ammo != WeaponManager.AMMO_INFINITE) {
			return false;
		}
		
		return weaponList[index].rechargeAmount == 0f;
	}
	
	public bool shoot(int index, Vector3 position, Quaternion rotation) {
		
		if(index > weaponList.Count) {
			return false;
		}
		
		if(isReady(index)) {
			if (weaponList[index].ammo > 0)
				weaponList[index].ammo--;

			weaponList[index].shootEvent(this, position, rotation);
			weaponList[index].rechargeAmount = weaponList[index].rechargeAmountMax;
			return true;
		}

		return false;
	}

	public bool refill(System.Type weaponType, int ammo) {
		foreach (var item in weaponList) {
			if (item.GetType() == weaponType) {
				item.ammo += ammo;
				return true;
			}
		}

		return false;
	}

	public void resetAllRecharge() {
		foreach(var item in weaponList) {
			item.rechargeAmount = 0f;
		}
	}
}

