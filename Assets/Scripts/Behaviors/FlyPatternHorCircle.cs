using UnityEngine;
using System.Collections;
using BehaviorLibrary;
using BehaviorLibrary.Components;
using BehaviorLibrary.Components.Composites;

public class FlyPatternHorCircle : MonoBehaviour {

	float elapsedTime = 0;
	
	enum MoveState {
		GOTO_CIRCLE,
		SPIN,
		MOVE_AWAY
	};
	MoveState moveState = MoveState.GOTO_CIRCLE;

	int speed = 5;

	Behavior behavior;

	// Use this for initialization
	IEnumerator Start() {
		
		//tree = BLBehaveLib.InstantiateTree(BLBehaveLib.TreeType.General_Hor, this);
		//behavior = new Behavior(root);

//		while (Application.isPlaying) {
//			yield return new WaitForSeconds(1f / GameManager.Frequency);
//			AIUpdate();
//		}
		yield return null;
	}

	public BehaviorReturnCode TickShootAction() {

		if (moveState == MoveState.SPIN) {
			return BehaviorReturnCode.Success;
		}

		if (Random.Range(0, 50) == 1) {
			(GetComponent(typeof(WeaponManager)) as WeaponManager).shoot(0, transform.position + Vector3.left * 2, Quaternion.identity);
		}

		return BehaviorReturnCode.Success;
	}

	public BehaviorReturnCode TickMoveAction() {

		elapsedTime += Time.deltaTime;

		switch (moveState) {
			case MoveState.GOTO_CIRCLE:

				if (elapsedTime > 1)
					moveState = MoveState.SPIN;
				break;
			case MoveState.SPIN:
				transform.Rotate(new Vector3(0, 2, 0));

				if (elapsedTime > 4)
					moveState = MoveState.MOVE_AWAY;
				break;
		}

		transform.Translate(Vector3.forward * speed * Time.deltaTime);

		return BehaviorReturnCode.Success;
	}

	BehaviorReturnCode Tick(bool init) {
		return BehaviorReturnCode.Success;
	}
}
