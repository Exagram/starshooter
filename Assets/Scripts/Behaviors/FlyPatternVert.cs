using UnityEngine;
using System.Collections;
using BehaviorLibrary;
using BehaviorLibrary.Components;
using BehaviorLibrary.Components.Composites;

public class FlyPatternVert : MonoBehaviour {

	public float waitTime = 0.5f;
	public float speedX = 5;
	public float speedY = 2;
	public int polarity = 1;
	public int randomShoot = 50;
	
	// Use this for initialization
	IEnumerator Start() {
		yield return new WaitForSeconds(waitTime);
	}

	void Update() {
		//Go to position
		if (waitTime > 0) {
			transform.Translate(Vector3.forward * speedX * Time.deltaTime);
			waitTime -= Time.deltaTime;
			return;
		}

		//And then go up/down
		if (transform.position.y > GameManager.BOUNDS_Y) {
			polarity *= -1;
		}

		if (transform.position.y < -GameManager.BOUNDS_Y) {
			polarity *= -1;
		}

		transform.Translate(Vector3.right * polarity * speedY * Time.deltaTime);

		if (Random.Range(0, randomShoot) == 0) {
			(GetComponent(typeof(WeaponManager)) as WeaponManager).shoot(0, transform.position + Vector3.left * 2, Quaternion.identity);
		}
	}
}
