using UnityEngine;
using System.Collections;
using BehaviorLibrary;
using BehaviorLibrary.Components;
using BehaviorLibrary.Components.Composites;

public class FlyPatternZigZag : MonoBehaviour {
	
	public int speed = 8;
	public float moveDirAxis = -1f;
	public float maxShootProb = 3000f;
	public Vector2 shootProb = new Vector2(0.5f, 1.0f);
	public float nextShootThreshold = 0;
	
	public Behavior behavior;
	public WeaponManager weaponManager;

	void Awake() {
		weaponManager = GetComponent(typeof(WeaponManager)) as WeaponManager;
	}

	IEnumerator Start() {
		yield return null;
	}
	
	public void setMoveAxis(float axis) {
		moveDirAxis = axis;
	}
	
	public BehaviorReturnCode TickTopBoundAction() {
		
		if(transform.position[1] > GameManager.BOUNDS_Y) {
			moveDirAxis = -1.0f;
			
			return BehaviorReturnCode.Success;
		} else {
			return BehaviorReturnCode.Failure;
		}
	}
	
	public BehaviorReturnCode TickBotBoundAction() {
		
		if(transform.position[1] < -GameManager.BOUNDS_Y) {
			moveDirAxis = 1.0f;
			
			return BehaviorReturnCode.Success;
		} else {
			return BehaviorReturnCode.Failure;
		}
	}

	public BehaviorReturnCode TickShootAction() {
		Debug.Log("NAY");

		if(nextShootThreshold < 0) {
			nextShootThreshold = Random.Range(shootProb.x, shootProb.y) * maxShootProb;

			weaponManager.shoot(0, transform.position + Vector3.left * 2, Quaternion.identity);
		} else {
			nextShootThreshold -= Time.deltaTime * maxShootProb;
		}
		
		return BehaviorReturnCode.Success;
	}
	
	public BehaviorReturnCode TickMoveAction() {
		Debug.Log("YAY");
		
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
		transform.Translate(Vector3.right * moveDirAxis * speed * Time.deltaTime);
		
		return BehaviorReturnCode.Success;
	}

	BehaviorReturnCode Tick (bool init) {
		return BehaviorReturnCode.Success;
	}
}
