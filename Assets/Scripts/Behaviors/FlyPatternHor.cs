using UnityEngine;
using System.Collections;
using BehaviorLibrary;
using BehaviorLibrary.Components;
using BehaviorLibrary.Components.Composites;
using BehaviorLibrary.Components.Actions;
using Skill.Framework.AI;

public class FlyPatternHor : MonoBehaviour, IFlyHorizontal {
	
	public int moveSpeed = 8;
	public float moveDirAxis = -1f;
	public Vector2 shootTimeWindow = new Vector2(200, 2000);

	float nextShootThreshold = 0;

	WeaponManager weaponManager;

	FlyHorizontalImpl behave;

	void Awake() {
		weaponManager = GetComponent<WeaponManager>();
		behave = new FlyHorizontalImpl(this);
		setNextShootThreshold();
	}

	void Update() {
		behave.Update();
	}

	void setNextShootThreshold() {
		nextShootThreshold = Random.Range(shootTimeWindow.x, shootTimeWindow.y);
	}

	#region IFlyHorizontal implementation
	
	BehaviorResult IFlyHorizontal.BasicMove (object sender, BehaviorParameterCollection c) {
		transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
		return BehaviorResult.Success;
	}
	
	BehaviorResult IFlyHorizontal.BasicShoot (object sender, BehaviorParameterCollection c) {
		if(nextShootThreshold < 0) {
			setNextShootThreshold();
			weaponManager.shoot(0, transform.position + Vector3.left * 2, Quaternion.identity);
		} else {
			nextShootThreshold -= Time.deltaTime * 1000;
		}
		return BehaviorResult.Success;
	}
	
	#endregion
}
