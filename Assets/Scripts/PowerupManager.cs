﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

public class PowerupManager {

	public enum ALL {
		Crate1,
	}

	private static Dictionary<string, GameObject> all = new Dictionary<string, GameObject>();

	public static void Initialize() {
		if (all.Count > 0) {
			return;
		}

		string[] names = Enum.GetNames(typeof(ALL));

		foreach (var item in names) {
			var obj = (GameObject)Resources.Load("Prefabs/" + item);
			all.Add(item, obj);
		}
	}

	public static GameObject find(string s) {
		return all[s];
	}

	public static GameObject find(ALL s) {
		return all[s.ToString()];
	}
}
