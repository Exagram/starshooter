using UnityEngine;
using System.Collections;

public class DeathRegion : MonoBehaviour {
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other) {
		other.SendMessageUpwards(SendMessages.onDeath, SendMessageOptions.DontRequireReceiver);
		
		if(other != null) {
			DestroyObject(other);
		}
	}
}
