using UnityEngine;
using System.Collections;

public class PlanetaryMotion : MonoBehaviour {

	public float speed;
	public Transform parent;

	void Start() {
	}

	// Update is called once per frame
	void Update () {
		if (parent == null) {
			transform.Rotate(Vector3.up, -speed * Time.deltaTime);
		} else {
			transform.RotateAround(parent.position, Vector3.up, -speed * Time.deltaTime);
		}
	}
}
