using System;
using System.Collections;

public class Tuple<T1, T2> : IEnumerable {
	public T1 t1;
	public T2 t2;

	public T1 Item1 {
		get {return t1;}
		set {t1 = value;}
	}

	public T2 Item2 {
		get {return t2;}
		set {t2 = value;}
	}

	public Tuple () {
	}
	
	public Tuple (T1 t1, T2 t2) {
		this.t1 = t1;
		this.t2 = t2;
	}

	public IEnumerator GetEnumerator() {
		yield return t1;
		yield return t2;
	}
}