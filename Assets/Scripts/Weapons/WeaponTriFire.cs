﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class WeaponTriFire : WeaponManager.Weapon {

	public WeaponTriFire() {
		ammo = 0;
		damageAmount = 2f;
		rechargeAmount = 0f;
		rechargeAmountMax = 0.5f;
		bulletObject = GameManager.prefabBulletPlayer;

		shootEvent = (Object parent, Vector3 position, Quaternion rotation) => {

			GameObject.Instantiate(bulletObject, position + 0.5f * Vector3.up, rotation);
			GameObject.Instantiate(bulletObject, position, rotation);
			GameObject.Instantiate(bulletObject, position + 0.5f * Vector3.down, rotation);
		};
	}

	public override string ToString () {
		return string.Format ("[WeaponTriFire]");
	}
}
