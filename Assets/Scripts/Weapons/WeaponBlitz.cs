﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class WeaponBlitz : WeaponManager.Weapon {

	public WeaponBlitz() {
		ammo = 0;
		damageAmount = 3f;
		rechargeAmount = 0f;
		rechargeAmountMax = 1.5f;
		bulletObject = GameManager.prefabBulletPlayer;

		shootEvent = (Object parent, Vector3 position, Quaternion rotation) => {

			GameObject obj;
			obj = (GameObject)GameObject.Instantiate(bulletObject, position + Vector3.up, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, 10)));
			obj.GetComponent<PlayerBullet>().homingSpeed = 8f;
			obj = (GameObject)GameObject.Instantiate(bulletObject, position + 0.5f * Vector3.up, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, 5)));
			obj.GetComponent<PlayerBullet>().homingSpeed = 8f;
			obj = (GameObject)GameObject.Instantiate(bulletObject, position, rotation);
			obj.GetComponent<PlayerBullet>().homingSpeed = 8f;
			obj = (GameObject)GameObject.Instantiate(bulletObject, position + 0.5f * Vector3.down, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, -5)));
			obj.GetComponent<PlayerBullet>().homingSpeed = 8f;
			obj = (GameObject)GameObject.Instantiate(bulletObject, position + Vector3.down, Quaternion.Euler(rotation.eulerAngles + new Vector3(0, 0, -10)));
			obj.GetComponent<PlayerBullet>().homingSpeed = 8f;
		};
	}

	public override string ToString () {
		return string.Format ("[WeaponBlitz]");
	}
}
