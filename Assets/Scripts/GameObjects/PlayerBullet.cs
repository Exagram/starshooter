using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBullet : MonoBehaviour {

	public float speed;
	public float homingSpeed = 0;
	public GameObject target;

	Life life;

	void Start() {
		if (GameManager.enemies.Count > 0) {
			var random = Random.Range(0, GameManager.enemies.Count - 1);
			target = GameManager.enemies[random];
		}

		life = GetComponent<Life>();
	}

	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.right * speed * Time.deltaTime);

		if (target != null) {
			transform.Translate(Vector3.up * (target.transform.position - transform.position).normalized.y * homingSpeed * Time.deltaTime);
		}
	}
	
	// gets called when we hit something
	void OnTriggerEnter(Collider other) {
		Debug.Log (other);
		var otherLife = other.GetComponentInParent<Life>();
		Debug.Log (otherLife);
		if(otherLife == null) {
			return;
		}

		if(other.gameObject.CompareTag(Tags.Player)) {
			otherLife.onDamage(1f / 4f);
			life.onDamage(1f / 4f);
		} else {
			otherLife.onDamage(1f);
			life.onDamage(1f);
		}
	}

	void onDeath() {
		Debug.Log("Player bullet onDeath");
		DestroyObject(gameObject);
	}
}
