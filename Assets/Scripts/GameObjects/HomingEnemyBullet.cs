using UnityEngine;
using System.Collections;
using System.Linq;

public class HomingEnemyBullet: MonoBehaviour {

	public float speed;
	public float homingSpeed;
	public GameObject target;
	public Life life;

	Vector3 translate;
	Vector3 skew;

	void Start() {
		life = GetComponent<Life>();
		life.maxHP = 1f;
		life.hp = 1f;

		if(target != null && homingSpeed != 0) {
			var players = GameManager.players.ToList().OrderBy(x => Vector3.Distance(gameObject.transform.position, x.transform.position));
			var l = players.ToList().Select (x => Vector3.Distance(gameObject.transform.position, x.transform.position).ToString()).ToArray();

			target = players.First();
		}
	}

	void Update () {
		if(target == null) {
			translate = Vector3.left;
		} else {
			skew = (target.transform.position.y - gameObject.transform.position.y) > 0 ?
				new Vector3(0, homingSpeed, 0)
					: new Vector3(0, -homingSpeed, 0);
			translate = (Vector3.left + skew).normalized;
		}

		transform.Translate(translate * speed * Time.deltaTime);
	}

	void OnTriggerEnter(Collider other) {
		foreach(var player in GameManager.players) {
			//other.gameObject.CompareTag("Player")

			if(other.gameObject == player) {
				var otherLife = player.GetComponentInParent<Life>();
				otherLife.onDamage(1f);
				life.onDamage(1f);
			}
		}
	}

	void onDeath() {
		Debug.Log("On death");
		DestroyObject(gameObject);
	}
}
