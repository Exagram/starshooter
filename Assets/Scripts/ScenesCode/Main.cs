using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {

	Texture2D logo, start, startDown, howto, howtoDown, exit, exitDown, help;
	int index = 0;
	int helpState = 0;

	const int offSet = 20;
	private int currY;

	void Awake() {
		GameManager.bootstrap();

		logo = Resources.Load("UI/logo") as Texture2D;
		start = Resources.Load("UI/btn-start") as Texture2D;
		startDown = Resources.Load("UI/btn-start-down") as Texture2D;
		howto = Resources.Load("UI/btn-howto") as Texture2D;
		howtoDown = Resources.Load("UI/btn-howto-down") as Texture2D;
		exit = Resources.Load("UI/btn-exit") as Texture2D;
		exitDown = Resources.Load("UI/btn-exit-down") as Texture2D;
		help = Resources.Load("UI/help") as Texture2D;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(Input.anyKeyDown + " " + helpState);
		if (Input.anyKeyDown && helpState == 1) {
			helpState = 0;
		}
		
		if (Input.GetButtonDown("P0.Vertical")) {
			index -= (Input.GetAxis("P0.Vertical") < 0) ? -1 : 1;
			index = (int)Mathf.Clamp(index, 0, 2);
		}

		if (Input.GetButtonDown("P0.Fire1")) {
			handleAction(index);
		}
	}

	void OnGUI() {
		currY = Screen.height / 10;
		GUI.DrawTexture(new Rect((Screen.width - logo.width) / 2, currY, logo.width, logo.height), logo);
		currY += logo.height + offSet;
		GUI.DrawTexture(new Rect((Screen.width - 300) / 2, currY, start.width, start.height), drawMenu(0));
		currY += start.height + offSet;
		GUI.DrawTexture(new Rect((Screen.width - 300) / 2, currY, howto.width, howto.height), drawMenu(1));
		currY += howto.height + offSet;
		GUI.DrawTexture(new Rect((Screen.width - 300) / 2, currY, exit.width, exit.height), drawMenu(2));
		currY += exit.height + offSet;

		if (helpState == 1) {
			GUI.DrawTexture(new Rect(Screen.width / 2 - help.width / 2, Screen.height / 2 - help.height / 2, help.width, help.height), help);
		}
	}

	Texture2D drawMenu(int i) {
		switch (i) {
			case 0:
				return (index == i) ? startDown : start;
			case 1:
				return (index == i) ? howtoDown : howto;
			case 2:
				return (index == i) ? exitDown : exit;
			default:
				return null;
		}
	}

	public void handleAction(int i) {
		switch (i) {
			case 0:
				GameManager.changeScenes("Scene1");
				break;
			case 1:
				Debug.Log("YAY");
				helpState = 1;
				break;
			case 2:
				Application.Quit();
				break;
		}
	}
}
