using UnityEngine;
using System.Collections;

public class Scene1 : MonoBehaviour {

	private static readonly float NormalSpawnX = GameManager.BOUNDS_X;
	private static readonly float NormalSpawnZ = 40;
	private static readonly Vector3 NormalSpawnPos = new Vector3(NormalSpawnX, 0, NormalSpawnZ);
	private static readonly Vector3 NormalSpawnRot = new Vector3(0, -90, 90);

	void Awake() {
		GameManager.bootstrap();
	}

	// Use this for initialization
	IEnumerator Start() {
		yield return null;

		for(int i = 0; i < GameManager.NumPlayers; i++) {
			var player = Instantiate(PrefabManager.find(PrefabManager.Types.Player), 
			                         new Vector3(-11f, i * GameManager.NumPlayers, NormalSpawnZ),
			                         Quaternion.Euler(-1f * NormalSpawnRot)
			                         ) as GameObject;
			GameManager.players.Add(player);

			var playerBehavior = GameManager.players[i].GetComponent(typeof(Player)) as Player;
			playerBehavior.playerIndex = i;
		}

		foreach(var player in GameManager.players) {
			DontDestroyOnLoad(player);
		}

		GameManager.hud = Camera.allCameras[0].GetComponent<HUD>();
		GameManager.musicManager.isPlaying = true;

		float shipApart = 2f;
		int lineCount = 0;
		int linesInWave = 0;

		yield return new WaitForSeconds(2f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, 
		                       typeof(FlyPatternHor), 
		                       NormalSpawnPos, 
		                       NormalSpawnRot);

		for(int k = 7; k < 9; k++) {
			yield return new WaitForSeconds(4f);
			lineCount = k * 2 + 1;
			linesInWave = 3;

			for(int i = 0; i < linesInWave; i++) {
				yield return new WaitForSeconds(1f);

				for(int j = 0; j < lineCount; j++) {
					GameManager.spawnEnemy(PrefabManager.Types.WaspHunter,
					                       typeof(FlyPatternHor), 
					                       new Vector3(NormalSpawnX, (-(lineCount*shipApart)/2 + j * shipApart)  * (1f + i / 2f), NormalSpawnZ), 
					                       NormalSpawnRot);
				}
			}
		}




		//Batch 1
		yield return new WaitForSeconds(50f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), NormalSpawnPos, NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), NormalSpawnPos, NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), NormalSpawnPos, NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);

		//Batch 2
		yield return new WaitForSeconds(10f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 8, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -8, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 6, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -6, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternZigZag), NormalSpawnPos, NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);

		//Batch 4
		yield return new WaitForSeconds(5f);
		GameManager.spawnEnemy(PrefabManager.Types.BigSpaceShip, typeof(FlyPatternVert), new Vector3(NormalSpawnX, GameManager.BOUNDS_Y, 0), NormalSpawnRot);
		for (int i = 5; i > -5; i--) {
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHorCircle), new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);
			yield return new WaitForSeconds(1f);
		}
		yield return new WaitForSeconds(5f);

		//Batch
		yield return new WaitForSeconds(5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 7, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 3, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -3, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -7, 0), NormalSpawnRot);

		//Powerup Trifire
		GameManager.spawnPowerup(PowerupManager.ALL.Crate1, null, new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);
		GameManager.currUnit.AddComponent<BlownLeft>().speed = 5;
		GameManager.currUnit.AddComponent<PlanetaryMotion>().speed = 1;
		GameManager.currUnit.AddComponent<PowerupTriFire>().amount = 15;

		//Batch
		yield return new WaitForSeconds(10f);
		GameManager.spawnEnemy(PrefabManager.Types.BigSpaceShip, typeof(FlyPatternVert), new Vector3(NormalSpawnX, GameManager.BOUNDS_Y, 0), NormalSpawnRot);

		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 7, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 3, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -3, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -7, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		for (int i = 6; i > -6; i -= 2) {
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, i, 0), NormalSpawnRot);
		}
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 8, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -8, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 7, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 3, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -3, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -7, 0), NormalSpawnRot);

		//Batch
		yield return new WaitForSeconds(10f);
		for (int i = 0; i < 5; i++) {
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 6, 0), NormalSpawnRot);
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 4, 0), NormalSpawnRot);
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 2, 0), NormalSpawnRot);
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -2, 0), NormalSpawnRot);
			GameManager.spawnEnemy(PrefabManager.Types.WaspHunter, typeof(FlyPatternHor), new Vector3(NormalSpawnX, -4, 0), NormalSpawnRot);
			yield return new WaitForSeconds(0.5f);
		}
		
		//Powerup
		yield return new WaitForSeconds(2f);
		GameManager.spawnPowerup(PowerupManager.ALL.Crate1, null, new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);
		GameManager.currUnit.AddComponent<BlownLeft>().speed = 5;
		GameManager.currUnit.AddComponent<PlanetaryMotion>().speed = 1;
		GameManager.currUnit.AddComponent<PowerupBlitz>().amount = 15;
		yield return new WaitForSeconds(1f);
		GameManager.spawnPowerup(PowerupManager.ALL.Crate1, null, new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);
		GameManager.currUnit.AddComponent<BlownLeft>().speed = 5;
		GameManager.currUnit.AddComponent<PlanetaryMotion>().speed = 1;
		GameManager.currUnit.AddComponent<PowerupHealth>().amount = 7;

		yield return new WaitForSeconds(10f);
		GameManager.spawnEnemy(PrefabManager.Types.BigSpaceShip, typeof(FlyPatternVert), new Vector3(NormalSpawnX, GameManager.BOUNDS_Y, 0), NormalSpawnRot);
		GameManager.spawnEnemy(PrefabManager.Types.BigSpaceShip, typeof(FlyPatternVert), new Vector3(NormalSpawnX, -GameManager.BOUNDS_Y, 0), NormalSpawnRot);
		yield return new WaitForSeconds(0.5f);
		GameManager.spawnEnemy(PrefabManager.Types.WaspHunterBoss, null, new Vector3(NormalSpawnX, 0, 0), NormalSpawnRot);
		FlyPatternVert pattern = GameManager.currUnit.AddComponent<FlyPatternVert>();
		pattern.speedX = 0.5f;
		pattern.speedY = 0.5f;
		pattern.randomShoot = 0;
	}
}
