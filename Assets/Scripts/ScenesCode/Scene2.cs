using UnityEngine;
using System.Collections;

public class Scene2 : MonoBehaviour {

	const float NORMAL_SPAWN_X = 16;
	readonly Vector3 NORMAL_SPAWN_POS = new Vector3(16, 4, 0);
	readonly Vector3 NORMAL_SPAWN_ROT = new Vector3(0, -90, 90);

	IEnumerator Start() {
		yield return null;

		GameManager.hud = Camera.allCameras[0].GetComponent<HUD>();

		GameManager.announce("Victory!", 5f);

		yield return new WaitForSeconds(5f);
		Debug.Log("DESTROYING: " + gameObject + " " + GameManager.instance);
		Destroy(gameObject);
		Debug.Log("DESTROYING: " + gameObject + " " + GameManager.instance);
		GameManager.changeScenes("Main");
		Debug.Log("DESTROYING: " + " " + GameManager.instance);
	}
}
