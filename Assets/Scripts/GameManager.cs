using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using BehaviorLibrary;
using BehaviorLibrary.Components;
using BehaviorLibrary.Components.Composites;
using BehaviorLibrary.Components.Actions;

public class GameManager : MonoBehaviour {
	
	public static readonly float BOUNDS_X = 31.0f;
	public static readonly float BOUNDS_Y = 18.0f;
	
	public static float Frequency = 1.0f;
	public static int NumPlayers = 1;
	
	public static BehaviorComponent getBehaviorTree() {
		return new RootSelector(null);
	}
	
	private static GameManager _instance = null;
	public static GameManager instance {
		get {
			return _instance;
		}
	}
	
	public static bool bootstrap() {
		//1.a Try to find it
		if(_instance == null) {
			_instance = FindObjectOfType(typeof(GameManager)) as GameManager;
			
			//1.b Else create it
			if (_instance == null) {
				var gameobject = new GameObject("GameManager");
				DontDestroyOnLoad(gameobject);
				
				_instance = gameobject.AddComponent<GameManager>();
				
			}
			
			Initialize();
			return _instance;
		}
		
		//2 Else return it
		return _instance != null;
	}
	
	public static GameObject currUnit;
	public static GameObject prefabBulletPlayer;
	public static GameObject prefabBulletEnemy;
	public static List<GameObject> players = new List<GameObject>();
	public static HUD hud;
	public static System.Random random = new System.Random();
	
	public static int level;
	
	public static bool isInitialized = false;
	
	public static MusicManager musicManager;
	public static List<GameObject> enemies = new List<GameObject>();
	
	public static void announce(string text, float life) {
		hud.Announce(text, life);
	}
	
	private struct PrefabToPath {
		public GameObject prefab;
		public string path;
		
		public PrefabToPath(GameObject prefab, string path) {
			this.prefab = prefab;
			this.path = path;
		}
	}
	
	public static void Initialize() {
		if(isInitialized) {
			return;
		} else {
			isInitialized = true;
		}
		
		prefabBulletEnemy = (GameObject)Resources.Load("Prefabs/BulletEnemy1");
		prefabBulletPlayer = (GameObject)Resources.Load("Prefabs/BulletPlayer1");
		
		PrefabManager.Initialize();
		PowerupManager.Initialize();
		SFXManager.Initialize();
		
		musicManager = _instance.gameObject.AddComponent<MusicManager>();
	}
	
	public static GameObject spawnEnemy(PrefabManager.Types spawnType, Type behaviour, Vector3 position, Vector3 rotation) {
		
		currUnit = Instantiate(PrefabManager.find(spawnType), position, Quaternion.Euler(rotation)) as GameObject;
		
		if (behaviour != null)
			currUnit.AddComponent(behaviour);
		
		enemies.Add(currUnit);
		return currUnit;
	}
	
	public static GameObject spawnPowerup(PowerupManager.ALL spawnType, Type behaviour, Vector3 position, Vector3 rotation) {
		
		currUnit = Instantiate(PowerupManager.find(spawnType), position, Quaternion.Euler(rotation)) as GameObject;
		
		if (behaviour != null)
			currUnit.AddComponent(behaviour);
		
		return currUnit;
	}
	
	public static void changeScenes(string newScene) {
		
		Application.LoadLevel(newScene);
	}
	
	public static Vector2 mapPointLerp(float i, float j) {
		
		return new Vector2{
			x = -BOUNDS_X + BOUNDS_X * 2 * i,
			y = -BOUNDS_Y + BOUNDS_Y * 2 * j
		};
	}
	
	public static Vector3 mapPointLerp(float i, float j, float z) {
		
		return new Vector3{
			x = -BOUNDS_X + BOUNDS_X * 2 * i,
			y = -BOUNDS_Y + BOUNDS_Y * 2 * j,
			z = z
		};
	}
}

