using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Skill.Framework.AI;
using AssemblyCSharp;

public interface IFlyHorizontal {
	BehaviorResult BasicMove(object sender, BehaviorParameterCollection c);
	BehaviorResult BasicShoot(object sender, BehaviorParameterCollection c);
}

public class FlyHorizontalImpl : FlyHorizontal {

	IFlyHorizontal IFlyHorizontal;
	IDictionary<Actions, SkillActionDelegate> actionsToSkillActions;

	public FlyHorizontalImpl (IFlyHorizontal IFlyHorizontal) {
		this.IFlyHorizontal = IFlyHorizontal;
		actionsToSkillActions = new Dictionary<Actions, SkillActionDelegate>{
			{Actions.BasicMove, IFlyHorizontal.BasicMove},
			{Actions.BasicShoot, IFlyHorizontal.BasicShoot}
		};

		CreateTree();

		UpdateTimeInterval = 0f;
		ContinuousUpdate = false;
		ChangeState("Root");
	}

	#region implemented abstract members of FlyHorizontal

	protected override BehaviorResult OnAction (Actions action, object sender, BehaviorParameterCollection parameters) {
		if(actionsToSkillActions.ContainsKey(action)) {
			return actionsToSkillActions[action](sender, parameters);
		} else {
			return BehaviorResult.Failure;
		}
	}

	protected override void OnActionReset (Actions action) {
	}

	protected override void OnStateChanged (string preState, string newState) {
		base.OnStateChanged (preState, newState);
	}

	#endregion
}