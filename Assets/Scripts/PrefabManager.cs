﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class PrefabManager {

	public enum Types {
		WaspHunter,
		WaspHunterBoss,
		BigSpaceShip,
		BigSpaceShipBoss,
		Player
	}

	private static Dictionary<string, GameObject> all = new Dictionary<string, GameObject>();

	public static void Initialize() {
		if (all.Count > 0) {
			return;
		}

		string[] names = Enum.GetNames(typeof(Types));

		foreach (var item in names) {
			var obj = (GameObject)Resources.Load("Prefabs/" + item);
			all.Add(item, obj);
		}
	}

	public static GameObject find(string s) {
		return all[s];
	}

	public static GameObject find(Types s) {
		return all[s.ToString()];
	}
}
