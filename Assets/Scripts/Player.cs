using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	private float moveSpeed = 8;
	
	public Life lifeComponent;
	public WeaponManager weaponManager;
	public int weaponIndex = 0;
	public int playerIndex = 0;
	string currInputName;

	// Use this for initialization
	void Start () {
		weaponManager = GetComponent(typeof(WeaponManager)) as WeaponManager;
		weaponManager.add(new WeaponManager.Weapon { 
			bulletObject = GameManager.prefabBulletPlayer,
			rechargeAmountMax = 0.1f,
		});
		weaponManager.add(new WeaponTriFire { });
		weaponManager.add(new WeaponBlitz { });
		
		lifeComponent = GetComponent(typeof(Life)) as Life;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(lifeComponent.liveState != Life.LiveState.ALIVE) {
			return;
		}

		currInputName = InputManager.ALL.Quit + "";
		if(Input.GetButtonDown(currInputName)) {
			Application.Quit();
			return;
		}

		currInputName = "P" + playerIndex + "." + InputManager.ALL.Fire1;
		if(Input.GetButtonDown(currInputName)) {
			bool isShot = weaponManager.shoot(weaponIndex, transform.position + Vector3.right * 2, Quaternion.identity);
			if (isShot) {
				audio.clip = SFXManager.find(SFXManager.ALL.Shoot.ToString());
				audio.Play();
			}
		}

		currInputName = "P" + playerIndex + "." + InputManager.ALL.Vertical;
		//transform.Translate(Vector3.left * Input.GetAxis(InputManager.ALL.Vertical.ToString()) * moveSpeed * Time.deltaTime);
		var verticalVal = Vector3.zero;
		if(Mathf.Abs(Input.GetAxis(currInputName)) > 0.1f) {
			verticalVal = Vector3.left * Input.GetAxis(currInputName);
		}

		currInputName = "P" + playerIndex + "." + InputManager.ALL.Horizontal;
		//transform.Translate(Vector3.left * Input.GetAxis(InputManager.ALL.Vertical.ToString()) * moveSpeed * Time.deltaTime);
		var horizontalVal = Vector3.zero;
		if(Mathf.Abs(Input.GetAxis(currInputName)) > 0.1f) {
			horizontalVal = Vector3.forward * Input.GetAxis(currInputName);
		}

		//transform.Translate(Vector3.Lerp(Vector3.zero, (verticalNormalized + horizontalNormalized) * moveSpeed, Time.deltaTime));
		transform.Translate((verticalVal + horizontalVal).normalized * moveSpeed * Time.deltaTime);

		currInputName = "P" + playerIndex + "." + InputManager.ALL.ChangeWeapons;
		if (Input.GetButtonDown(currInputName)) {
			if (Input.GetAxis(currInputName) > 0 && weaponIndex < weaponManager.weaponList.Count - 1) {
				weaponIndex++;
			}
			if (Input.GetAxis(currInputName) < 0 && weaponIndex > 0) {
				weaponIndex--;
			}
		}

		if(transform.position[1] > GameManager.BOUNDS_Y) {
			transform.position -= new Vector3{y = transform.position[1] - GameManager.BOUNDS_Y};
		}
		
		if(transform.position[1] < -GameManager.BOUNDS_Y) {
			transform.position -= new Vector3{y = transform.position[1] + GameManager.BOUNDS_Y};
		}

		if(transform.position[0] > GameManager.BOUNDS_X) {
			transform.position -= new Vector3{x = transform.position[0] - GameManager.BOUNDS_X};
		}
		
		if(transform.position[0] < -GameManager.BOUNDS_X) {
			transform.position -= new Vector3{x = transform.position[0] + GameManager.BOUNDS_X};
		}
	}
	
	// gets called when we hit something
	void OnTriggerEnter(Collider other) {
		
		if(other.gameObject.CompareTag("Enemy")) {
			//lifeComponent.onDamage(1.0f);
		}
	}
	
	void onDeath() {

		audio.clip = SFXManager.find(SFXManager.ALL.ExplosionPlayer);
		audio.Play();
		GameManager.musicManager.musicIndex = 0;
		GameManager.musicManager.isPlaying = false;

		Debug.Log("onDeath");

		int living = 0;
		foreach(var player in GameManager.players) {
			var life = player.GetComponent<Life>();
			if(life.liveState == Life.LiveState.ALIVE) {
				living += 1;
			}
		}
		if(living == 0) {
			GameManager.announce("Game over!", 5f);
			StartCoroutine(deathSequence());
		}
	}
	
	IEnumerator deathSequence() {
		
		Debug.Log("deathSeq");
		
		yield return new WaitForSeconds(2f);
		GameManager.changeScenes("Main");
	}
}
