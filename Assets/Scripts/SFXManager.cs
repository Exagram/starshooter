﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SFXManager {

	public enum ALL {
		ExplosionEnemy,
		ExplosionPlayer,
		HitEnemy,
		Shoot
	}

	private static Dictionary<string, AudioClip> all = new Dictionary<string, AudioClip>();

	public static void Initialize() {
		if (all.Count > 0) {
			return;
		}

		string[] names = Enum.GetNames(typeof(ALL));

		foreach (var item in names) {
			var obj = (AudioClip)Resources.Load("SFX/" + item);
			all.Add(item, obj);
		}
	}

	public static AudioClip find(string s) {
		return all[s];
	}

	public static AudioClip find(ALL s) {
		return all[s.ToString()];
	}
}
