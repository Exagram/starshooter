using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlownLeft : MonoBehaviour {
	public float speed;

	void Update() {
		transform.Translate(transform.InverseTransformDirection(Vector3.left)  * speed * Time.deltaTime);
	}
}

