using UnityEngine;
using System.Collections;

public class Life : MonoBehaviour {
	
	public float hp;
	public float regen;
	public float maxHP;
	
	public enum LiveState {
		ALIVE,
		DYING,
		DEAD
	}
	public LiveState liveState = LiveState.ALIVE;
	
	// Use this for initialization
	void Start () {
		hp = maxHP;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(liveState != LiveState.ALIVE) {
			return;
		}
		
		if(hp < 0) {
			gameObject.SendMessage(SendMessages.onDeath, null, SendMessageOptions.RequireReceiver);
			return;
		}
		
		hp = Mathf.Clamp(hp + regen * Time.deltaTime, 0, maxHP);
	}
	
	public void onDamage(float damage) {
		
		if(liveState != LiveState.ALIVE) {
			return;
		}
		
		hp -= damage;
		
		if(hp <= 0) {
			liveState = LiveState.DYING;
			gameObject.SendMessage(SendMessages.onDeath, null, SendMessageOptions.RequireReceiver);
		}
	}
}

