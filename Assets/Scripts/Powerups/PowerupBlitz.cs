﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PowerupBlitz : MonoBehaviour {

	public int amount = 10;

	void OnTriggerEnter(Collider other) {
		Player player = other.GetComponent<Player>();

		if (player != null) {
			player.weaponManager.refill(typeof(WeaponBlitz), amount);
			Destroy(gameObject);
		}
	}
}
