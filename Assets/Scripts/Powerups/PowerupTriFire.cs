﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PowerupTriFire : MonoBehaviour {

	public int amount = 10;

	void OnTriggerEnter(Collider other) {
		Player player = other.GetComponent<Player>();

		Debug.Log("YUMMY 1: " + player + " " + other);
		Debug.Log("YUMMY 1: " + player + " " + other.name);
		if (player != null) {
			Debug.Log("YUMMY 2 " + player.weaponManager.weaponList[1].ammo);
			player.weaponManager.refill(typeof(WeaponTriFire), amount);
			Debug.Log("YUMMY 3 " + player.weaponManager.weaponList[1].ammo);
			Destroy(gameObject);
		}
	}
}
