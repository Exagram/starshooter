﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PowerupHealth : MonoBehaviour {

	public int amount = 1;

	void OnTriggerEnter(Collider other) {
		Player player = other.GetComponent<Player>();

		if (player != null) {
			player.lifeComponent.maxHP += amount;
			player.lifeComponent.hp = player.lifeComponent.maxHP;
			Destroy(gameObject);
		}
	}
}
