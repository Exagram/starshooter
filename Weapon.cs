using UnityEngine;
using System.Collections;

public class Weapon {
	
	public GameObject gameObject;
	
	public float damageAmount = 0f;
	public float rechargeAmount = 0f;
	public float rechargeAmountMax = 0f;
	
	public void update(float amount) {
		rechargeAmount = Mathf.Clamp(rechargeAmount - amount, 0, rechargeAmountMax);
	}
}

